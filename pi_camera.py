import os
import sys
import subprocess
import time

def install_camera():
    subprocess.check_output(["sudo", "apt-get", "install", "fswebcam"])
    
    
def run_camera(image, N_images):
    for images, index in enumerate(N_images):
        image = images+index
        subprocess.check_output(["fswebcam", "%s.jpg"%image])
        time.sleep(1)
    
install_camera()


